//
//  UIAlertAction+Extensions.swift
//
//  Created by Jorge Villalobos on 06/02/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit

public typealias AlertAction = (UIAlertAction) -> Void

public extension UIAlertAction {
    
    public convenience init(title: String?, style: UIAlertActionStyle, imageAction: UIImage?, handler: AlertAction? = nil) {
        self.init(title: title, style: style, handler: handler)
        if let image = imageAction {
            self.actionImage = image
        }
    }
    
    public var actionImage: UIImage {
        get {
            return self.value(forKey: "image") as? UIImage ?? UIImage()
        }
        set(image) {
            self.setValue(image, forKey: "image")
        }
    }
}
