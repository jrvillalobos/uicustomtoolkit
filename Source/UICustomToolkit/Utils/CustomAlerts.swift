//
//  CustomAlerts.swift
//
//  Created by Jorge Villalobos on 05/09/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit
import CommonsAssets

public class CustomAlerts {
    
    public class func getAlert(title: BundleMessage? = DefaultAlertMessages.empty, message: BundleMessage?, actions: [AlertActions] = [.standard(DefaultAlertActionMessages.okAction, nil)], preferredStyle: UIAlertControllerStyle = .alert, withPreferredAction: Bool = false) -> UIAlertController {
        let alertController = UIAlertController(title: self.getValue(title), message: self.getValue(message), preferredStyle: preferredStyle)
        let alertActions = actions.map { action in
            action.getAction()
        }
        alertActions.forEach(alertController.addAction)
        if withPreferredAction {
            if #available(iOS 9.0, *) {
                alertController.preferredAction = alertActions[0]
            }
        }
        return alertController
    }
    
    private class func getValue(_ message: BundleMessage?) -> String? {
        if let message = message {
            return message.getMessage()
        } else {
            return nil
        }
    }
}
