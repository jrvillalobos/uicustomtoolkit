//
//  AlertActionTypes.swift
//
//  Created by Jorge Villalobos on 05/09/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import CommonsAssets
import UIKit

public enum AlertActionConstants: CustomStringConvertible {
    
    case configuration
    case error
    
    public var description: String {
        switch self {
        case .configuration:
            return "AlertNotificationMessages"
        case .error:
            return "AlertNotificationMessagesErrors"
        }
    }
}

public enum AlertActions {
    
    case standard(AlertActionMessage, AlertAction?)
    case cancel(AlertActionMessage, AlertAction?)
    case destructive(AlertActionMessage, AlertAction?)
    
    public func getAction() -> UIAlertAction {
        switch self {
        case .standard(let title, let handler):
            return UIAlertAction.init(title: title.getMessage(), style: .default, imageAction: title.image, handler: handler)
        case .cancel(let title, let handler):
            return UIAlertAction(title: title.getMessage(), style: .cancel, imageAction: title.image, handler: handler)
        case .destructive(let title, let handler):
            return UIAlertAction(title: title.getMessage(), style: .destructive, imageAction: title.image, handler: handler)
        }
    }
}

public protocol AlertActionMessage: BundleMessage {
    
    var image: UIImage? { get }
}


public extension AlertActionMessage {
    
    public var bundleId: String {
        return AlertActionConstants.configuration.description
    }
    
    public var image: UIImage? {
        return nil
    }
}

public enum DefaultAlertActionMessages: AlertActionMessage {
    
    case openSettingsAction
    case settingsAction
    case continueAction
    case okAction
    case cancelAction
    
    public var key: String {
        switch self {
        case .openSettingsAction:
            return "changeSettingsAction"
        case .settingsAction:
            return "settingsAction"
        case .continueAction:
            return "continueAction"
        case .okAction:
            return "okAction"
        case .cancelAction:
            return "cancelAction"
        }
    }
}

public enum DefaultAlertMessages: BundleMessage {
    
    case plist(String)
    case text(String)
    case empty
    
    public var bundleId: String {
        return AlertActionConstants.configuration.description
    }
    
    public var key: String {
        switch self {
        case .plist(let key):
            return key
        case .text(_), .empty:
            return ""
        }
    }
    
    public func getMessage(_ args: [CVarArg] = []) -> String {
        switch self {
        case .empty:
            return ""
        case .text(let message):
            return message.formatter(args: args)
        default:
            let message : String! = BundleFacade.shared.displayMessage(bundleId: self.bundleId, key: self.key, args: args)
            return message
        }
    }
}
