//
//  Country.swift
//
//  Created by Jorge Villalobos on 30/08/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation

struct Country {
    
	var code: String?
	var name: String?
	var phoneCode: String?
	var flag: UIImage?
	
	init(code: String?, name: String?, phoneCode: String?) {
		self.code = code
		self.name = name
		self.phoneCode = phoneCode
		
		if let code = code, let flag = UIImage(named: code, in: Bundle.UICustomToolkitIcons, compatibleWith: nil) {
			self.flag = flag
		} else {
			self.flag = UIImage(named: "unknown", in: Bundle.UICustomToolkitIcons, compatibleWith: nil)
		}
	}
}
