//
//  RoundedButton.swift
//
//  Created by Jorge Villalobos on 25/10/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

@IBDesignable
public class RoundedButton:  ClickableButton {
	
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
	override public func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 2/UIScreen.main.nativeScale
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        if #available(iOS 10.0, *) {
            titleLabel?.adjustsFontForContentSizeCategory = true
        } else {
         titleLabel!.numberOfLines = 1
         titleLabel!.adjustsFontSizeToFitWidth = true
         titleLabel!.baselineAdjustment = .alignCenters
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10.0
        layer.borderColor = isEnabled ? tintColor.cgColor : UIColor.clear.cgColor
    }
}
