//
//  ClickableButton.swift
//
//  Created by Jorge Villalobos on 31/08/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class ClickableButton:  UIButton {
    
    @IBInspectable
    public var titleColorHighlighted: UIColor? {
        didSet {
            self.setTitleColor(self.titleColorHighlighted, for: .highlighted)
        }
    }
    
    @IBInspectable
    public var imageHighlighted: UIImage? {
        didSet {
            self.setImage(self.imageHighlighted, for: .highlighted)
        }
    }
    
    @IBInspectable
    public var backgroundColorHighlighted: UIColor? {
        didSet {
            self.setBackgroundColor(color: self.backgroundColorHighlighted!, forState: .highlighted)
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
}
