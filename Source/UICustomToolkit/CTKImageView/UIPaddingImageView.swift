//
//  UIPaddingImageView.swift
//
//  Created by Jorge Villalobos on 24/10/17.
//  Copyright © 2017 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit
import CommonsAssets

@IBDesignable
public class UIPaddingImageView: UIImageView {
	
	@IBInspectable
    public var paddingTop: CGFloat = 0
    
	@IBInspectable
    public var paddingBottom: CGFloat = 0
    
	@IBInspectable
    public var paddingLeft: CGFloat = 0
    
	@IBInspectable
    public var paddingRight: CGFloat = 0
	
	override public func prepareForInterfaceBuilder() {
		setupPadding()
	}
	
	override public func awakeFromNib() {
		setupPadding()
	}
	
	private func setupPadding() {
		self.image = self.image?.addImagePadding(top: paddingTop, right: paddingRight, bottom: paddingBottom, left: paddingLeft)
	}
}
