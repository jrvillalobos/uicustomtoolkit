//
//  CTKMultimediaPickerController.swift
//
//  Created by Jorge Villalobos on 04/09/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit
import Photos
import AssetsLibrary
import CommonsAssets
import MobileCoreServices

@objc open class CTKMultimediaPickerController: NSObject {
    
    fileprivate var actions: [AlertActions]
    
    open var delegate: CTKMultimediaPickerDelegate?
    open var maxSize: Int = 0 // KB's
    open var editablePicker: Bool = false
    open var documentsDirectory: Bool = false
    open var messagesDirectory: String = ""
    
    open var deviceHasCamera: Bool {
        return UIImagePickerController.isSourceTypeAvailable(.camera)
    }
    
    public init(delegate: CTKMultimediaPickerDelegate? = nil) {
        self.delegate = delegate
        self.actions = []
    }
    
    public func showMenuOptions(pickers: [CTKPickerAction]) {
        self.configureAlertActions(pickers: pickers)
        self.delegate?.presentAlertController(alertController: CustomAlerts.getAlert(title: nil, message: nil, actions: self.actions, preferredStyle: .actionSheet), successful: true)
    }
    
}

extension CTKMultimediaPickerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismissPicker(byImage: picker, wasCancelled: false)
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        let uiImagePickerControllerReferenceURL = info[UIImagePickerControllerReferenceURL] as? URL
        let uiImagePickerControllerMediaURL = info[UIImagePickerControllerMediaURL] as? URL
        let editedImage: UIImage? = (self.editablePicker) ? info[UIImagePickerControllerEditedImage] as? UIImage : nil
        let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if mediaType.isEqual(to: kUTTypeImage as NSString as String) {
            if let photoAlbumURL = uiImagePickerControllerReferenceURL { // User have choosed an image
                // The image came from photo album
                self.addImageFromURLToViewController(photoAlbumURL, editedImage, type: .image)
            } else {
                // The image came from camera
                if let image = originalImage {
                    self.addImageFromCameraToViewController(image, editedImage, type: .image)
                } else {
                    self.presentAlertErrorInUpload()
                }
            }
        } else if mediaType.isEqual(to: kUTTypeMovie as NSString as String) {
            // User have choosed an video
            if let videoURL = uiImagePickerControllerMediaURL {
                self.addVideoFromURLToViewController(videoURL, type: .video)
            } else {
                self.presentAlertErrorInUpload()
            }
            
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismissPicker(byImage: picker, wasCancelled: true)
    }
}

extension CTKMultimediaPickerController: UIDocumentMenuDelegate {
    
    // MARK: UIDocumentPickerMenu
    public func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.presentPicker(byDocument: documentPicker)
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        self.dismissPicker(byDocument: documentMenu, wasCancelled: true)
    }
}

extension CTKMultimediaPickerController: UIDocumentPickerDelegate {
    
    // MARK: UIDocumentPickerDelegate
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        self.addDocumentFromURLToViewController(url, type: .file)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.dismissPicker(byDocument: controller, wasCancelled: true)
    }
}

fileprivate extension CTKMultimediaPickerController {
    
    fileprivate func configureAlertActions(pickers: [CTKPickerAction]) {
        self.actions = pickers.map() { pickerAction -> AlertActions in
            let alertAction: AlertActions
            switch pickerAction {
            case .camera, .onlyPhotoCamera:
                alertAction = .standard(CTKPickerAlertActionMessages.openCameraAction, { [unowned self] _ in
                    self.configurePickerForCamera(pickerAction: pickerAction)
                })
            case .onlyPhotoLibrary:
                alertAction = .standard(CTKPickerAlertActionMessages.openGalleryAction, { [unowned self] _ in
                    self.configurePickerForPhotoLibrary(pickerAction: pickerAction)
                })
            case .photoLibrary, .savedPhotosAlbum:
                alertAction = .standard(CTKPickerAlertActionMessages.openPhotosVideosAction, { [unowned self] _ in
                    self.configurePickerForPhotoLibrary(pickerAction: pickerAction)
                })
            case .importDocument:
                alertAction = .standard(CTKPickerAlertActionMessages.openDocumentsAction, { [unowned self] _ in
                    self.configureDocumentPicker(pickerAction: pickerAction)
                })
            }
            return alertAction
        }
        let cancelAction: AlertActions = .cancel(DefaultAlertActionMessages.cancelAction, { [unowned self] _ in
            self.delegate = nil
        })
        self.actions.append(cancelAction)
    }
    
    private func configurePickerForCamera(pickerAction: CTKPickerAction) {
        if self.deviceHasCamera {
            let picker: UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = pickerAction.source
            picker.allowsEditing = editablePicker
            if pickerAction.onlyImage {
                picker.cameraCaptureMode = .photo
                picker.mediaTypes = [kUTTypeImage as String]
                picker.modalPresentationStyle = .fullScreen
            } else {
                picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
            }
            self.presentPicker(byImage: picker)
        } else {
            self.showAlertNotFoundCamera()
        }
    }
    
    private func configurePickerForPhotoLibrary(pickerAction: CTKPickerAction) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker: UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = pickerAction.source
            picker.allowsEditing = editablePicker
            if pickerAction.onlyImage {
                picker.mediaTypes = [kUTTypeImage as String]
                picker.modalPresentationStyle = .popover
            } else {
                picker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
            }
            self.presentPicker(byImage: picker)
        }
    }
    
    private func configureDocumentPicker(pickerAction: CTKPickerAction) {
        let allowedDocumentTypes:[String] = [
            String(kUTTypeText),
            String(kUTTypeCompositeContent),
            String(kUTTypePlainText),
            String(kUTTypeUTF8PlainText),
            String(kUTTypeJPEG),
            String(kUTTypeTIFF),
            String(kUTTypePNG),
            String(kUTTypeAudio),
            String(kUTTypeApplication),
            String(kUTTypeGIF),
            String(kUTTypeBMP),
            String(kUTTypePDF),
            String(kUTTypeHTML),
            String(kUTTypeXML),
            String(kUTTypeSpreadsheet),
            String(kUTTypePresentation),
            String(kUTTypeDatabase),
            ]
        let picker = UIDocumentPickerViewController(documentTypes: allowedDocumentTypes, in: pickerAction.mode)
        picker.delegate = self
        self.presentPicker(byDocument: picker)
    }
    
    fileprivate func presentPicker(byDocument picker: UIDocumentPickerViewController) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.presentPicker(byDocument: picker)
        }
    }
    
    fileprivate func presentPicker(byImage picker: UIImagePickerController) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.presentPicker(byImage: picker)
        }
    }
    
    fileprivate func dismissPicker(byImage picker: UIImagePickerController, wasCancelled: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.dismissPicker(byImage: picker, wasCancelled: wasCancelled)
            if wasCancelled {
                self?.delegate = nil
            }
        }
    }
    
    fileprivate func dismissPicker(byDocument picker: UIViewController, wasCancelled: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.dismissPicker(byDocument: picker, wasCancelled: wasCancelled)
            if wasCancelled {
                self?.delegate = nil
            }
        }
    }
    
    fileprivate func showAlertNotFoundCamera() {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.presentAlertController(alertController: CustomAlerts.getAlert(title: CTKPickerAlertMessages.deviceWithoutCameraTitle, message: CTKPickerAlertMessages.deviceWithoutCameraMsg), successful: false)
        }
    }
    
    fileprivate func presentAlertErrorInUpload() {
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.presentAlertController(alertController: CustomAlerts.getAlert(message: CTKPickerAlertMessages.incorrectFileUploadErrorMsg), successful: false)
        }
    }
    
    fileprivate func presentAlertErrorFileSizeExceeded() {
        DispatchQueue.main.async { [weak self] in
            let maxSizeMB: String = "\((self?.maxSize)!/1024)"
            self?.delegate?.presentAlertController(alertController: CustomAlerts.getAlert(message: DefaultAlertMessages.text(CTKPickerAlertMessages.fileSizeExceededMSg.getMessage(maxSizeMB))), successful: false)
        }
    }
    
    private typealias File = (URL, String, Data, UIImage?)
    
    fileprivate func addImageFromURLToViewController(_ url: URL, _ editedImage: UIImage?, type: MultimediaType) {
        guard var file = self.getFile(by: url, editedImage: editedImage),
            let urlTemp = copyBundleResourceToTemporaryDirectory(filename: file.1, data: file.2) else {
                return
        }
        
        file.0 = urlTemp
        self.onSendFile(file: file, type: type)
    }
    
    fileprivate func addImageFromCameraToViewController(_ image: UIImage, _ editedImage: UIImage?, type: MultimediaType) {
        let orientation = ALAssetOrientation(rawValue: image.imageOrientation.rawValue)
        ALAssetsLibrary().writeImage(toSavedPhotosAlbum: image.cgImage, orientation: orientation!) { (url: URL?, error: Error?) in
            
            guard var file = self.getFile(by: url, editedImage: editedImage),
                let urlTemp = self.copyBundleResourceToTemporaryDirectory(filename: file.1, data: file.2) else {
                    return
            }
            
            file.0 = urlTemp
            self.onSendFile(file: file, type: type)
        }
    }
    
    fileprivate func addVideoFromURLToViewController(_ urlVideo: URL, type: MultimediaType) {
        let filename = urlVideo.lastPathComponent
        guard let data = try? Data(contentsOf: urlVideo) else {
            self.presentAlertErrorInUpload()
            return
        }
        
        let size = data.count/1024
        
        if self.maxSize > 0 && size > self.maxSize {
            self.presentAlertErrorFileSizeExceeded()
            return
        }
        
        guard let urlTemp = copyBundleResourceToTemporaryDirectory(filename: filename, data: data) else {
            self.presentAlertErrorInUpload()
            return
        }
        
        let file: File = (urlTemp, filename, data, nil)
        self.onSendFile(file: file, type: type)
    }
    
    fileprivate func addDocumentFromURLToViewController(_ url: URL, type: MultimediaType) {
        let filename = url.lastPathComponent
        
        guard let data = try? Data(contentsOf: url) else {
            self.presentAlertErrorInUpload()
            return
        }
        
        let size = data.count/1024
        
        if self.maxSize > 0 && size > self.maxSize {
            self.presentAlertErrorFileSizeExceeded()
            return
        }
        
        guard  let urlTemp = copyBundleResourceToTemporaryDirectory(filename: filename, data: data) else {
            self.presentAlertErrorInUpload()
            return
        }
        
        let file: File = (urlTemp, filename, data, nil)
        self.onSendFile(file: file, type: type)
    }
    
    private func onSendFile(file: File, type: MultimediaType) {
        let dataImage: Data
        if let editedImage = file.3, let editedData = UIImageJPEGRepresentation(editedImage, 1) {
            dataImage = editedData
        } else {
            dataImage = file.2
        }
        let size = dataImage.count/1024
        if self.maxSize > 0 && size > self.maxSize {
            self.presentAlertErrorFileSizeExceeded()
            return
        }
        let contentType: String = MimeType(url: file.0).value
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.mediaPickerController(url: file.0, fileName: file.1, data: file.2, editedImage: file.3, contentType: contentType, type: type)
        }
    }
    
    private func getFile(by url: URL?, editedImage: UIImage?) -> File? {
        guard let url = url, let asset = self.obtainAssetForURL(url), let filename = self.obtainNameFileInAsset(asset), let data = self.obtainDataFromAsset(asset: asset) else {
            self.presentAlertErrorInUpload()
            return nil
        }
        return (url, filename, data, editedImage)
    }
    
    private func obtainAssetForURL(_ url: URL) -> PHAsset? {
        let assets = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
        var asset: PHAsset? = nil
        if let firstAsset = assets.firstObject {
            asset = firstAsset
        }
        return asset
    }
    
    private func obtainNameFileInAsset(_ asset: PHAsset) -> String? {
        var fileName: String? = nil
        if let firstResource = PHAssetResource.assetResources(for: asset).first {
            fileName = firstResource.originalFilename
        }
        return fileName
    }
    
    private func obtainDataFromAsset(asset: PHAsset) -> Data? {
        var data: Data? = nil
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        PHImageManager.default().requestImageData(for: asset, options: options) {
            (imageData, dataURI, orientation, info) -> Void in
            if let imageDataExists = imageData {
                data = imageDataExists
            }
        }
        return data
    }
    
    private func copyBundleResourceToTemporaryDirectory(filename: String, data: Data) -> URL? {
        do {
            var urlTemp: URL!
            if self.documentsDirectory {
                urlTemp = createPathInDocumentDirectory(filename: filename)
                
            } else {
                urlTemp = createTempDirectory(filename: filename)
            }
            
            try data.write(to: urlTemp!, options: .atomic)
            return urlTemp
            
        } catch let error {
            print("Unable to copy file: \(error)")
        }
        return nil
    }
    
    private func createTempDirectory(filename: String) -> URL? {
        guard let tempDirURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename, isDirectory: false) else {
            return nil
        }
        
        return tempDirURL
    }
    
    private func createPathInDocumentDirectory(filename: String) -> URL? {
        let filemgr = FileManager.default
        let dirPaths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        
        let docsDir = dirPaths[0]
        
        var dirURL = docsDir
            .appendingPathComponent("Directrips", isDirectory: true)
            .appendingPathComponent(self.messagesDirectory, isDirectory: true)
        
        do {
            try filemgr.createDirectory(atPath: dirURL.path, withIntermediateDirectories: true, attributes: nil)
            dirURL = dirURL.appendingPathComponent(filename, isDirectory: false)
        }
        catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
            return nil
        }
        
        return dirURL
    }
}

