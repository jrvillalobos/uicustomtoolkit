//
//  CTKMultimediaPickerDelegate.swift
//
//  Created by Jorge Villalobos on 05/09/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit

public protocol CTKMultimediaPickerDelegate {
    
    func mediaPickerController(url: URL, fileName: String, data: Data, editedImage: UIImage?, contentType: String, type: MultimediaType)
    
    func presentPicker(byImage picker: UIImagePickerController)
    
    func presentPicker(byDocument picker: UIDocumentPickerViewController)
    
    func dismissPicker(byImage picker: UIImagePickerController, wasCancelled: Bool)
    
    func dismissPicker(byDocument picker: UIViewController, wasCancelled: Bool)
    
    func presentAlertController(alertController: UIAlertController, successful: Bool)
}

public extension CTKMultimediaPickerDelegate where Self: UIViewController {
    
    public func presentPicker(byImage picker: UIImagePickerController) {
        DispatchQueue.main.async {
            self.present(picker, animated: true, completion: nil)
        }
    }
    public func dismissPicker(byImage picker: UIImagePickerController, wasCancelled: Bool) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    public func presentPicker(byDocument picker: UIDocumentPickerViewController) {
        DispatchQueue.main.async {
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    public func dismissPicker(byDocument picker: UIViewController, wasCancelled: Bool) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    public func presentAlertController(alertController: UIAlertController, successful: Bool) {
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
}
