//
//  CTKPickerTypes.swift
//
//  Created by Jorge Villalobos on 05/09/18.
//  Copyright © 2018 Desarrollo FVP. All rights reserved.
//

import Foundation
import UIKit
import CommonsAssets

public enum MultimediaType {
    
    case audio
    case video
    case file
    case image
}

public enum CTKPickerAction {
    
    case camera
    case onlyPhotoCamera
    case photoLibrary
    case onlyPhotoLibrary
    case savedPhotosAlbum
    case importDocument
    
    public var onlyImage: Bool {
        switch self {
        case .onlyPhotoCamera, .onlyPhotoLibrary:
            return true
        default:
            return false
        }
    }
    
    public var source: UIImagePickerControllerSourceType! {
        switch self {
        case .camera:
            return .camera
        case .onlyPhotoCamera:
            return .camera
        case .photoLibrary:
            return .photoLibrary
        case .onlyPhotoLibrary:
            return .photoLibrary
        case .savedPhotosAlbum:
            return .savedPhotosAlbum
        case .importDocument:
            return nil
        }
    }
    
    public var mode: UIDocumentPickerMode! {
        switch self {
        case .importDocument:
            return .import
        default:
            return nil
        }
    }
}

public enum CTKPickerAlertActionMessages: AlertActionMessage {
    
    case openGalleryAction
    case openPhotosVideosAction
    case openDocumentsAction
    case openCameraAction
    
    public var image: UIImage? {
        let resourceName: String
        switch self {
        case .openCameraAction:
            resourceName = "ic_picker_camera"
        case .openDocumentsAction:
            resourceName = "ic_picker_document"
        case .openPhotosVideosAction, .openGalleryAction:
            resourceName = "ic_picker_gallery"
        }
        return UIImage(named: resourceName, in: Bundle.CTKMultimediaPicker(), compatibleWith: nil)
    }
    
    public var key: String {
        switch self {
        case .openGalleryAction:
            return "openGalleryAction"
        case .openPhotosVideosAction:
            return "openPhotosVideosAction"
        case .openDocumentsAction:
            return "openDocumentsAction"
        case .openCameraAction:
            return "openCameraAction"
        }
    }
}

public enum CTKPickerAlertMessages: BundleMessage {
    
    case deviceWithoutCameraTitle
    case deviceWithoutCameraMsg
    case fileSizeExceededMSg
    case incorrectFileUploadErrorMsg
    
    public var bundleId: String {
        return AlertActionConstants.configuration.description
    }
    
    public var key: String {
        switch self {
        case .fileSizeExceededMSg:
            return "fileSizeExceededMSg"
        case .incorrectFileUploadErrorMsg:
            return "incorrectFileUploadErrorMsg"
        case .deviceWithoutCameraTitle:
            return "deviceWithoutCameraTitle"
        case .deviceWithoutCameraMsg:
            return "deviceWithoutCameraMsg"
        }
    }
}
